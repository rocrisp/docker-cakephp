docker build --no-cache -t quay.io/rocrisp/democakephpapp .
docker push quay.io/rocrisp/democakephpapp
oc delete deploymentconfig.apps.openshift.io/demo
oc run demo --image=quay.io/rocrisp/democakephpapp:latest